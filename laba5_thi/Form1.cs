﻿using StonesLibrary.ClassLibrary1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace laba5_thi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            if (AddForm.Result == DialogResult.OK)
            {
                AbstractWeapon weapon = AddForm.Weapon;
                this.weaponListBox.Items.Add(weapon);
                division.AddWeapon(weapon);
            }

        }


        public class AddEditForm
        {
            InitializeComponent();
            buttonOK.Visible = false;
            Stone = obj[index];
            type = (obj[index].GetType().ToString().Substring(14));
            FillBoxes(Stone);

        }

        private AbstractStone _stone;
        public AbstractWeapon Stone
        {
            get { return _stone; }
            set { _stone = value; }
        }
        private DialogResult _result;
        public DialogResult Result
        {
            get { return _result; }
            set { _result = value; }
        }
        private void okButton_Click(object sender, EventArgs e)
        {
            Stone stone1 = new Stone(); //new Blade() ....
                                        //инициализация полей объекта Weapon из полей ввода формы
            stone1.Name = nameTextBox.Text;
            Result = DialogResult.OK;
        }


        private void buttonAdd_Click(object sender, EventArgs e)
        {
            Form2 newForm = new Form2();
            newForm.Show();
        }

        private void buttonEdit_Click(object sender, EventArgs e)
        {
            Form2 newForm = new Form2();
            newForm.Show();
        }

        private void listStones_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void totalPrice_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}




private void CancelButton_Click(object sender, EventArgs e)
{
    Result = DialogResult.Cancel;
}